# -*- coding: utf-8 -*-
from django import forms
from django.core.exceptions import ValidationError
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from comments.models import Comment


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        exclude = ('name', 'time')

    def clean_text(self):
        text = self.cleaned_data.get('text')
        if not text or not len(text):
            raise ValidationError("Комментарий не введен")
        return text


class LoginForm(forms.Form):
    username = forms.CharField(label=(u'Логин'))
    password = forms.CharField(label=(u'Пароль'))

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache= authenticate(username=username,password=password)
            if self.user_cache is None:
                raise forms.ValidationError("Неверное сочетание логина и пароля.")
            elif not self.user_cache.is_active:
                raise forms.ValidationError("Пользователь заблокирован.")

        return self.cleaned_data

    def clean_username(self):
        username = self.cleaned_data['username']
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            raise forms.ValidationError("Пользователя не существует.")
        return username
