# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^', include('comments.urls')),
#    url(r'^comments/', include('comments.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
