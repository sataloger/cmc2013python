# -*- coding: utf-8 -*-
import logging
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.core.urlresolvers import reverse

from .models import Comment
from .forms import CommentForm,  LoginForm

baselog = logging.getLogger('comments')
log = baselog.getChild(__name__)


@login_required
def index(request):
    log.warning(request.method)
    if request.method == "POST" and request.POST:
        form = CommentForm(request.POST)

        if form.is_valid():
            comment = form.save(commit=False)
            comment.name = request.user.username
            comment.save()
            return HttpResponseRedirect(reverse('comments.views.index'))
        else:
            pass
    else:
        form = CommentForm()

    comments = Comment.objects.all().order_by('-time')
    return render_to_response('index.html', locals(), RequestContext(request))


def register(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()

            username = form.cleaned_data['username']
            password=form.cleaned_data['password1']
            user = auth.authenticate(username=username, password=password)

            if user is not None and user.is_active:
                auth.login(request, user)

            return HttpResponseRedirect(reverse('comments.views.index'))
    else:
        form = UserCreationForm()

    return render_to_response('register.html', locals(), RequestContext(request))


def login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('comments.views.index'))

    if request.method == "POST":
        form = LoginForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = auth.authenticate(username=username, password=password)

            if user is not None and user.is_active:
                auth.login(request, user)
                return HttpResponseRedirect(reverse("comments.views.index"))
    else:
        form = LoginForm()

    return render_to_response('login.html', locals(), RequestContext(request))


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse("comments.views.index"))