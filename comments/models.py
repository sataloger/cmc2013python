# -*- coding: utf-8 -*-
from django.db import models

class Comment(models.Model):
    name = models.CharField(max_length=30, blank=True, verbose_name=u'Имя')
    text = models.TextField(blank=True, verbose_name=u'Текст комментария')
    time = models.DateTimeField(auto_now=True, verbose_name=u'Время')

    class Meta:
        verbose_name=u'Комментарй'
        verbose_name_plural=u'Комментарии'

    def __unicode__(self):
        return "%s : %s" % (self.name, self.text)

