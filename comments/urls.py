# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^$', 'comments.views.index', name='index'),
    url(r'^register/', 'comments.views.register', name='register'),
    url(r'^login/', 'comments.views.login', name='login'),
    url(r'^logout/', 'comments.views.logout', name='logout'),
)