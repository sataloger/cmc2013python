# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Comment

class CommentAdmin(admin.ModelAdmin):
    list_display = ['name', 'text', 'time']

admin.site.register(Comment, CommentAdmin)